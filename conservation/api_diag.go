package conservation

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"
)

type Diag struct {
	Gbif          int     `json:"gbif"`
	Restcountries int     `json:"restcountries"`
	Version       string  `json:"version"`
	Uptime        float64 `json:"uptime"`
}

type Time struct {
	StartTime time.Time
	Uptime    float64
}

//Uptime
func (t *Time) getStatus(w http.ResponseWriter, url string) int {

	result, err := http.Get(url)
	if err != nil {
		http.Error(w, "Could not retrieve from server", http.StatusBadRequest)
		t.StartTime = time.Now()
	}
	t.Uptime = time.Since(t.StartTime).Seconds()
	return result.StatusCode
}

//Formats the output
func (di Diag) format(w http.ResponseWriter) {
	json_di, err := json.MarshalIndent(di, "", "    ")
	if err != nil {
		http.Error(w, "Could not encode the object", http.StatusBadRequest)
	}
	fmt.Fprint(w, string(json_di))
}

func HandlerDiag(w http.ResponseWriter, r *http.Request) {
	//Timer for uptime
	timer := Time{
		StartTime: time.Now(),
	}

	parts := strings.Split(r.URL.Path, "/")
	version := parts[2]

	urlGbif := "https://api.gbif.org/v1/species/"
	urlRestcountries := "https://restcountries.eu/rest/v2/"

	var di Diag

	di.Gbif = timer.getStatus(w, urlGbif)
	di.Restcountries = timer.getStatus(w, urlRestcountries)

	di.Version = version

	di.Uptime = timer.Uptime

	di.format(w)

}
