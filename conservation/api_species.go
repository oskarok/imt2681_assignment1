package conservation

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
)

type Species struct {
	Key            int    `json:"key"`
	Kingdom        string `json:"kingdom"`
	Phylum         string `json:"phylum"`
	Order          string `json:"order"`
	Family         string `json:"family"`
	Genus          string `json:"genus"`
	ScientificName string `json:"scientificName"`
	CanonicalName  string `json:"canonicalName"`
	Year           string `json:"bracketYear"`
}

type Year struct {
	Number string `json:"bracketYear"`
}

//Formats the output
func (s Species) format(w http.ResponseWriter) {
	json_s, err := json.MarshalIndent(s, "", "    ")
	if err != nil {
		http.Error(w, "Could not encode the object", http.StatusBadRequest)
	}
	fmt.Fprint(w, string(json_s))
}

func HandlerSpecies(w http.ResponseWriter, r *http.Request) {
	parts := strings.Split(r.URL.Path, "/")
	key := parts[4]

	urlSpecies := "https://api.gbif.org/v1/species/" + key + "/"   //Species info
	urlYear := "https://api.gbif.org/v1/species/" + key + "/name/" //Year

	var s Species
	err := json.NewDecoder(getApi(w, urlSpecies)).Decode(&s)
	if err != nil {
		http.Error(w, "Could not decode the response from client", http.StatusBadRequest)
	}
	err = json.NewDecoder(getApi(w, urlYear)).Decode(&s)
	if err != nil {
		http.Error(w, "Could not decode the response from client", http.StatusBadRequest)
	}

	s.format(w)

}
