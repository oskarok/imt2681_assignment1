package conservation

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
)

type Country struct {
	Code       string   `json:"alpha2Code"`
	Name       string   `json:"name"`
	Flag       string   `json:"flag"`
	Species    []string `json:"species"`
	SpeciesKey []int    `json:"speciesKey"`
}

type Speciesid struct {
	Results []struct {
		Species    string `json:"species"`
		SpeciesKey int    `json:"speciesKey"`
	} `json:"results"`
}

//Checks for duplicates
func (c *Country) duplicate(s string) bool {
	for _, x := range c.Species {
		if x == s {
			return true
		}
	}
	return false
}

//Gets data from url
func getApi(w http.ResponseWriter, url string) io.Reader {
	resp, err := http.Get(url)
	if err != nil {
		http.Error(w, "Could not request server", http.StatusNotFound)
	}
	return resp.Body
}

//Formats the output, makes it look good
func (c Country) format(w http.ResponseWriter) {
	json_c, err := json.MarshalIndent(c, "", "    ")
	if err != nil {
		http.Error(w, "Could not encode object", http.StatusBadRequest)
	}
	fmt.Fprint(w, string(json_c))
}

func HandlerCountry(w http.ResponseWriter, r *http.Request) {
	parts := strings.Split(r.URL.Path, "/")
	countryIdentifier := parts[4] // ISO number the user inputs
	limit := parts[5]             // limit of occurrences

	urlCountry := "https://restcountries.eu/rest/v2/alpha/" + countryIdentifier
	urlSpecies := "https://api.gbif.org/v1/occurrence/search?country=" +
		countryIdentifier + "&limit=" + limit

	var c Country
	var sid Speciesid
	err := json.NewDecoder(getApi(w, urlCountry)).Decode(&c) //Get country data
	if err != nil {
		http.Error(w, "Could not decode the response from client", http.StatusBadRequest)
	}
	err = json.NewDecoder(getApi(w, urlSpecies)).Decode(&sid) //Get species data
	if err != nil {
		http.Error(w, "Could not decode the response from client", http.StatusBadRequest)
	}

	for _, s := range sid.Results {
		if c.duplicate(s.Species) != true { //Checking for dupes
			c.Species = append(c.Species, s.Species)
			c.SpeciesKey = append(c.SpeciesKey, s.SpeciesKey)
		}
	}

	c.format(w)

}
